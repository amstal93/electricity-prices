With this script you can get the electricity prices (in Spain), adapted to the new PVPC tariff. 
Also send the best/worst hours to Telegram. 
This script uses the API from ESIOS, and you need to request a token sending an email to consultasios@ree.es

1. Create .env file:

```
ESIOS_TOKEN=
TELEGRAM_TOKEN=
TELEGRAM_CHAT_ID=
```

2. Run:

```shell
docker build --tag electricity-prices .
```

3. Run:

```shell
docker run -it --env-file /path/to/env/file/.env --rm electricity-prices app.py [OPTIONS]
```

If you want to run with a cron job, use:

```shell
docker run --env-file /path/to/env/file/.env --rm electricity-prices app.py [OPTIONS]
```

Options:

- -hours, --num_hours: How many hour do you want to receive. Default: 24
- -start, --start_hour: At what hour (in 24 hour format) do you want to start your day (and take in consideration for the best/worst hours). Default: -1 (all hours)
- -order, --order: Choose if you want to receive the list ordered by "hour" or by "price". Default: hour

4. If you want to run the script without Docker:

```shell
pip install -r requirements.txt
python app.py [OPTIONS]
```

5. Additionally, if you want to save the best hours into a txt file for another stuff (as use a [Dynamic Cron Script](https://gitlab.com/tmllull/random-scripts/-/tree/master/Dynamic%20Cron)), you need to redirect the output to a file (docker run --env-file /path/to/env/file/.env --rm electricity-prices app.py > besthours.txt) and use the follow script (you can found it into repo too):

```bash
#!/bin/bash

input="besthours.txt"
while IFS= read -r line
do
if [[ $line == *"Worst"* ]]; then
    break
fi
if [[ $line == *"-"* ]]; then
    hours=($(echo $line | tr "-" "\n"))
    hour=${hours[0]}
    list_hours=$list_hours","$hour
fi
done < "$input"
echo "${list_hours:1}" > myCleanedHours.txt
```
